# BTC→BTS Converter

![Tests](https://bitbucket.org/luna_koly/btc2btsconverter/downloads/main-tests-status.svg)

What is BTC?
That's easy - Bitcoins!
What is BTS?
Everyone knows - it's a popular Korean boy band!

If you have a bitcoin or few - how do you convert them to a BTS song?
Seems, there're no services of such a kind, so the niche is free, and it's time to fill it.

## Run

Run everything with:

```sh
docker-compose up
```

To run the containers with additional ports & directories bindings, use:

```sh
docker-compose -f docker-compose.dev.yml up
```

To run unit tests, issue:

```sh
docker-compose -f docker-compose.test.yml up --abort-on-container-exit
```

If the containers are running already, simply use:

```sh
docker exec -it btc2btsconverter-backend-1 python manage.py test
```

Use the `--build` and `--force-recreate` options to ensure the containers are up-to-date.

## Use
### Web

Navigate to `localhost:6969` and check if the conversion works.

Navigate to `localhost:8000/admin` to play around with the data. Resources are available as `localhost:8000/api/resources`.

### CLI

Use the following `curl` commands to send a request to the backend via the command line:

```sh
curl -H 'Accept: application/json; indent=2' -d "value=11" http://127.0.0.1:8000/api/btc2bts/
```

Use `-u root:0000` to perform the request as admin:

```sh
curl -H 'Accept: application/json; indent=2' -u root:0000 http://127.0.0.1:8000/api/random-bts-song/
```

## Fun

### Waiting for Containers

The `docker-compose`'s `depends_on` feature is not enough when working with `postgres`: the `db` starts before it's ready for use, but if the Django `backend` starts before the database is ready, it ~~shits~~ shuts down.

This is solved by running the `wait-for-postgres.sh` script that simply blocks until the database is ready.
To make the script work properly inside the container, it must first be made runnable (`chmod +x`) on the host machine, and then committed to Git. To ensure the backend container sees the files _properly_, all files are saved with LF-endings.

### Mounting Project Directory

Since Python stores its dependencies _somewhere_ outside the project directory, the backend works fine when mounting the project directory to the host machine. The frontend fails to start, because after mounting the `node_modules` folder becomes no longer available.

### Run Command within Container

It's possible to run a command within a container with something like this:

```sh
docker exec -it btc2btsconverter-backend-1 python manage.py migrate
```

### Database Initialization

We can easily [create](https://www.postgresqltutorial.com/postgresql-backup-database/) a database initialization script with this command:

```sh
pg_dump -U postgres -W -F p -h localhost -p 5432 postgres > init.sql
```

### Requests per Second

It's possible to measure the number of requests per second with:

```sh
ab -n 100 -c 100 -A root:0000 http://localhost:8000/api/random-bts-song/
```

For some reason, it shows the same value no matter whether Redis is used or not (yea, caching a random value is weird, but quite illustrative).

We can verify Redis is running by:

```sh
redis-cli -p 6378 keys \*
```

