if [[ "$BITBUCKET_EXIT_CODE" != "0" ]]; then
    python3 -m pybadges --left-text=Tests --right-text=Failing --right-color="#d00" > main-tests-status.svg
else
    python3 -m pybadges --left-text=Tests --right-text=Passing --right-color="#0d0" > main-tests-status.svg
fi
