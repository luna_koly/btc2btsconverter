import {
  BrowserRouter as Router,
  Routes,
  Route,
} from "react-router-dom"

import { Btc2Bts } from "../Btc2Bts"

interface AppProps {

}

export function App(props: AppProps) {
  return (
    <div
      style={{
        backgroundColor: '#191919',
        color: 'white',
        minHeight: '100vh',
      }}
    >
      <Router>
        <Routes>
          <Route path="" element={<Btc2Bts style={{ height: '100vh' }}/>} />
          <Route path="*" element={<div>Nah</div>} />
        </Routes>
      </Router>
    </div>
  )
}
