import React from 'react'

export interface AlbumCoverProps {
  cover: string
  overlay?: string
  fade?: number
  style: React.CSSProperties
}

export function AlbumCover(props: AlbumCoverProps) {
  const fade = props.fade ?? 0

  return (
    <div
      style={{
        borderRadius: '10px',
        boxShadow: '0 0 50px #50ffde',
        position: 'relative',
        overflow: 'hidden',

        transform: `rotateZ(${360 * fade}deg)`,
        transition: 'transform 1s ease',

        ...props.style
      }}
    >
      <img
        src={props.cover}
        style={{
          width: '100%',
          height: '100%',
          position: 'absolute',
          left: 0,
          top: 0,
        }}
      />

      <img
        src={props.overlay}
        style={{
          width: '100%',
          height: '100%',
          position: 'absolute',
          left: 0,
          top: 0,

          opacity: fade,
          transition: 'opacity 1s ease',
        }}
      />
    </div>
  )
}
