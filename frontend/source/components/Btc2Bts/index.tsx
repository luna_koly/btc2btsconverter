import React, { KeyboardEventHandler, useEffect, useState } from 'react'

import {
  Button,
  TextField,
} from '@mui/material'

import { toast } from 'material-react-toastify'
import { useMediaQuery } from '@mui/material'

import { t } from '../../i18next'
import { useActions, useAppSelector } from '../../hooks'

import { AlbumCover } from '../AlbumCover'

import tradeOffer from '../../images/trade-offer-fade.png'
import ricardo from '../../images/ricardo-fade.png'
import mistery from '../../images/mistery.svg'

interface ColumnProps {
  basis: string,
  children: React.ReactNode
  style?: React.CSSProperties
  hideOverflow?: boolean
}

function Column(props: ColumnProps) {
  return (
    <div
      style={{
        flexBasis: props.basis,
        flexDirection: 'column',
        transition: 'flex-basis 1s ease',

        overflow: props.hideOverflow ? 'hidden' : 'none',
        minWidth: 0,

        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',

        ...props.style,
      }}
    >
      {props.children}
    </div>
  )
}

interface BackgroundBannerProps {
  image: string
  opacity: number
}

function BackgroundBanner(props: BackgroundBannerProps) {
  return (
    <div
      style={{
        backgroundImage: `url(${props.image})`,
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        backgroundSize: 'auto 100%',

        opacity: props.opacity,
        height: '100%',
        width: '100%',

        position: 'absolute',
        left: '50%',
        top: '0',

        transform: 'translateX(-50%)',
        transition: 'opacity 1.5s ease'
      }}
    />
  )
}

interface Btc2BtsProps {
  style?: React.CSSProperties
}

function youtubeLinkToId(link: string) {
  const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
  const match = link.match(regExp);

  return (match && match[2].length === 11)
    ? match[2]
    : null;
}

function youtubeLinkToEmbedded(link: string) {
  return 'http://www.youtube.com/embed/' + youtubeLinkToId(link)
}

export function Btc2Bts(props: Btc2BtsProps) {
  const { value, song } = useAppSelector(state => state.btc2bts)
  const { convert, reset } = useActions()
  const [ text, setText ] = useState('')

  function onEnter() {
    const number = parseFloat(text)

    if (isNaN(number)) {
      toast.info(t('This is not a valid number'))
    } else {
      convert(number)
    }
  }

  function onKeyDown(event: any) {
    if (event.key == 'Enter') {
      onEnter()
    }
  }

  function isLoading() {
    return value != null && song == null
  }

  function getTradeOfferOpacity() {
    return isLoading() || song ? 0 : 1
  }

  function getRicardoOpacity() {
    return song ? 1 : 0
  }

  const showSmall = useMediaQuery('(max-width: 570px)')

  function getFlexDirection() {
    if (showSmall) {
      return 'column'
    }

    return 'row'
  }

  function getValueBasis() {
    if (!song) {
      return '50%'
    }

    return '0%'
  }

  function getCoverBasis() {
    if (!song) {
      return '50%'
    }

    return '30%'
  }

  function getVideoBasis() {
    if (!song) {
      return '0%'
    }

    return '70%'
  }

  function getEmbeddedLink() {
    if (song) {
      return youtubeLinkToEmbedded(song.youtube)
    }

    return undefined
  }

  function getAlbumCoverFade() {
    if (song?.album) {
      return 1
    }

    return 0
  }

  const [ oldSong, setOldSong ] = useState<typeof song | null>(null)

  function rememberSongAndReset() {
    setOldSong(song)
    reset()
  }

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: getFlexDirection(),
        alignItems: 'stretch',

        ...props.style
      }}
    >
      <BackgroundBanner image={ricardo} opacity={getRicardoOpacity()} />
      <BackgroundBanner image={tradeOffer} opacity={getTradeOfferOpacity()} />

      <Column basis={getValueBasis()} hideOverflow>
        <TextField
          label={t('Some Bitcoins')}
          value={text}
          onChange={event => setText(event.target.value)}
          onKeyPress={onKeyDown}
          disabled={isLoading()}
        />
      </Column>

      <Column basis={getCoverBasis()}>
        <AlbumCover
          cover={mistery}
          overlay={song?.album?.cover ?? oldSong?.album?.cover}
          fade={getAlbumCoverFade()}
          style={{
            width: '10em',
            height: '10em',
            zIndex: 2,
          }}
        />
      </Column>

      <Column basis={getVideoBasis()} hideOverflow>
        <iframe
          width="560"
          height="315"
          src={getEmbeddedLink()}
          title="YouTube video player"
          frameBorder="0"
          // allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen
          style={{
            width: '78%',
            height: '60%',
            zIndex: 2,
          }}
        />

        <Button
          variant="outlined"
          size="large"
          style={{
            margin: '2em',
          }}
          onClick={rememberSongAndReset}
        >
          {t('I changed my mind')}
        </Button>
      </Column>
    </div>
  )
}
