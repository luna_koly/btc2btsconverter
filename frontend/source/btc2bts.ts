export interface Album {
    title: string
    cover: string
}

export interface Song {
    title: string
    album: Album | null
    youtube: string
}
