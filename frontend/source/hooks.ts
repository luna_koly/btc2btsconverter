import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux'
import { bindActionCreators } from 'redux'

import { RootState, AppDispatch } from './store'
import * as Btc2BtsActionCreators from './action-creators/btc2bts'

export const useAppDispatch = () => useDispatch<AppDispatch>()

export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector

export const useActions = () => {
    const dispatch = useAppDispatch()
    return bindActionCreators(Btc2BtsActionCreators, dispatch)
}
