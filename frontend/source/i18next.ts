import i18n from 'i18next'
import LanguageDetector from 'i18next-browser-languagedetector'

i18n.use(LanguageDetector).init({
  resources: {
    en: {
      translations: {
        'This is not a valid number': 'This is not a valid number',
        'Some Bitcoins': 'Some Bitcoins',
        'I changed my mind': 'I changed my mind',
      }
    },
    ru: {
      translations: {
        'This is not a valid number': 'Это не число',
        'Some Bitcoins': 'Биткоины',
        'I changed my mind': 'Я передумал',
      }
    }
  },

  fallbackLng: "en",
  debug: true,

  // Have a common namespace used around the full app
  ns: ["translations"],
  defaultNS: "translations",

  keySeparator: false, // We use content as keys

  interpolation: {
    escapeValue: false, // Not needed for react!!
    formatSeparator: ","
  },

  react: {
    wait: true
  }
})

export { i18n }

export const t = i18n.t
