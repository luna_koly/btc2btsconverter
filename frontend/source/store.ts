
import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit'

import { btc2btsReducer } from './reducers/btc2bts'

export const store = configureStore({
  reducer: {
    btc2bts: btc2btsReducer,
  },
})

export type AppDispatch = typeof store.dispatch

export type RootState = ReturnType<typeof store.getState>

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>
