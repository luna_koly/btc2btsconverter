import { toast } from 'material-react-toastify'

import { Song } from '../btc2bts'
import { t } from '../i18next'

export interface Btc2BtsState {
  value: number | null
  song: Song | null
}

export enum Btc2BtsActionTypes {
  CONVERTION = 'CONVERTION',
  CONVERTION_SUCCESS = 'CONVERTION_SUCCESS',
  CONVERTION_FAILURE = 'CONVERTION_FAILURE',
  RESET = 'RESET',
}

export interface Btc2BtsConvertionAction {
  type: Btc2BtsActionTypes.CONVERTION
  value: number
}

export interface Btc2BtsConvertionSuccessAction {
  type: Btc2BtsActionTypes.CONVERTION_SUCCESS
  song: Song
}

export interface Btc2BtsConvertionFailureAction {
  type: Btc2BtsActionTypes.CONVERTION_FAILURE
}

export interface Btc2BtsReset {
  type: Btc2BtsActionTypes.RESET
}

export type Btc2BtsAction = Btc2BtsConvertionAction
  | Btc2BtsConvertionSuccessAction
  | Btc2BtsConvertionFailureAction
  | Btc2BtsReset

const initialState: Btc2BtsState = {
  value: null,
  song: null,
}

export function btc2btsReducer(
  state = initialState,
  action: Btc2BtsAction
): Btc2BtsState {
  switch (action.type) {
    case Btc2BtsActionTypes.CONVERTION:
      return { ...state, value: action.value }
    case Btc2BtsActionTypes.CONVERTION_SUCCESS:
      return { ...state, song: action.song }
    case Btc2BtsActionTypes.CONVERTION_FAILURE:
      toast.error(t('Sorry, Convertion Error'))
      return { ...state, value: null }
    case Btc2BtsActionTypes.RESET:
      return { ...state, value: null, song: null }
    default:
      return state
  }
}
