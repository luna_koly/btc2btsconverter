import { Dispatch } from 'react'

import axios from 'axios'

import { Btc2BtsAction, Btc2BtsActionTypes } from '../reducers/btc2bts'

export function convert(value: number) {
  return async (dispatch: Dispatch<Btc2BtsAction>) => {
    try {
      dispatch({ type: Btc2BtsActionTypes.CONVERTION, value: value })

      const response = await axios.post(
        'http://localhost:8000/api/btc2bts/',
        {
          value: value,
        }
      )

      dispatch({ type: Btc2BtsActionTypes.CONVERTION_SUCCESS, song: response.data })
    } catch (error) {
      dispatch({ type: Btc2BtsActionTypes.CONVERTION_FAILURE })
    }
  }
}

export function reset() {
  return (dispatch: Dispatch<Btc2BtsAction>) => {
    dispatch({ type: Btc2BtsActionTypes.RESET })
  }
}
