import React from 'react'
import ReactDOM from 'react-dom'

import { Provider } from 'react-redux'
import { I18nextProvider } from 'react-i18next'
import { ToastContainer } from 'material-react-toastify'
import { createTheme, ThemeProvider } from '@mui/material'

import { store } from './store'
import { i18n } from './i18next'

import { App } from './components/App'

import './styles/global.scss'
import 'material-react-toastify/dist/ReactToastify.css'

const theme = createTheme({
  palette: {
    mode: 'dark',
  },
})

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <I18nextProvider i18n={i18n}>
        <ThemeProvider theme={theme}>
          <App />
        </ThemeProvider>
        <ToastContainer />
      </I18nextProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
)
