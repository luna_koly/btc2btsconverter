const path = require('path')

// import HtmlWebpackPlugin from 'html-webpack-plugin'
// "type": "module",

const HtmlWebpackPlugin = require('html-webpack-plugin')

// export const mode =

module.exports = {
  mode: 'development',
  entry: './source/index.tsx',
  output: {
    path: path.resolve(__dirname, 'build'),
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx']
  },
  module: {
    rules: [
      {
        test: /\.(css|sass|scss)$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
      {
        test: /\.(jpg|jpeg|png|svg)$/,
        use: 'file-loader',
      },
      {
        test: /\.(ts|tsx)$/,
        use: 'ts-loader',
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({ template: './public/index.html' }),
  ],
  devServer: {
    historyApiFallback: true,
  },
}
