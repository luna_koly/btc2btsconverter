import random

from rest_framework import viewsets, views, permissions, status
from rest_framework.response import Response
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page

from .models import Album, Tag, Song, SongTag
from .serializers import *

# Create your views here.

class AlbumViewSet(viewsets.ModelViewSet):
    queryset = Album.objects.all()
    serializer_class = AlbumSerializer
    permission_classes = [permissions.IsAdminUser]

class TagViewSet(viewsets.ModelViewSet):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    permission_classes = [permissions.IsAdminUser]

class SongViewSet(viewsets.ModelViewSet):
    queryset = Song.objects.all()
    serializer_class = SongSerializer
    permission_classes = [permissions.IsAdminUser]

class SongTagViewSet(viewsets.ModelViewSet):
    queryset = SongTag.objects.all()
    serializer_class = SongTagSerializer
    permission_classes = [permissions.IsAdminUser]

class RandomBtsSong(views.APIView):
    permission_classes = [permissions.IsAuthenticated]

    @method_decorator(cache_page(60 * 1))
    def get(self, request):
        songs = list(Song.objects.all())
        random_song = random.sample(songs, 1)[0]

        serializer = ExplicitSongSerializer(random_song, context={
            'request': request,
        })

        return Response(serializer.data)

def digits_only(text):
    is_digit = lambda it: '0' <= it and it <= '9'
    return ''.join(filter(is_digit, text))

def hash_numeric(value):
    digits = digits_only(str(value))

    if digits == '':
        return 0

    return int(digits)

class Btc2Bts(views.APIView):
    serializer_class = BtcReport

    def get(self, request):
        return Response({
            'detail': 'Use "POST" to express how many BTC you have',
        })

    def post(self, request):
        input_serializer = self.serializer_class(data=request.data)

        if not input_serializer.is_valid():
            return Response(input_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        songs_count = Song.objects.all().count()
        value = hash_numeric(input_serializer.data['value'])
        modulo = value % songs_count
        random_song = Song.objects.all()[modulo]

        output_serializer = ExplicitSongSerializer(random_song, context={
            'request': request,
        })

        # TODO: For some reason, upon returning an object
        # the HTML form for SongSerializer is also rendered,
        # and it replaces the BtcReport serializer form.
        # I have no idea how to fix it.
        return Response(output_serializer.data)
