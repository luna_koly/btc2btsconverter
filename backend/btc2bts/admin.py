from django.contrib import admin
from .models import Album, Tag, Song, SongTag

# Register your models here.

@admin.register(Album)
class AlbumAdmin(admin.ModelAdmin):
    pass

@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    pass

@admin.register(Song)
class SongAdmin(admin.ModelAdmin):
    pass

@admin.register(SongTag)
class SongTagAdmin(admin.ModelAdmin):
    list_display = ('id', 'song', 'tag')
