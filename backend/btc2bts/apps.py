from django.apps import AppConfig

class Btc2BtsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'btc2bts'
