from django.db.models import fields
from rest_framework import serializers

from .models import Album, Tag, Song, SongTag

class AlbumSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Album
        fields = '__all__'

class TagSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Tag
        fields = '__all__'

class SongSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Song
        fields = '__all__'

class SongTagSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = SongTag
        fields = '__all__'

class ExplicitSongSerializer(serializers.HyperlinkedModelSerializer):
    album = AlbumSerializer()

    class Meta:
        model = Song
        fields = '__all__'

class BtcReport(serializers.Serializer):
    value = serializers.FloatField()
