from django.db import models

# Create your models here.

class Album(models.Model):
    title = models.TextField()
    cover = models.ImageField(upload_to='album-covers', null=True)

    def __str__(self):
        return f'[{self.id}] {self.title}'

class Tag(models.Model):
    title = models.TextField()

    def __str__(self):
        return f'[{self.id}] {self.title}'

class Song(models.Model):
    title = models.TextField()
    album = models.ForeignKey(Album, on_delete=models.CASCADE, null=True)
    youtube = models.TextField()

    def __str__(self):
        return f'[{self.id}] {self.title}'

class SongTag(models.Model):
    song = models.ForeignKey(Song, on_delete=models.CASCADE)
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)

    def __str__(self):
        return f'[{self.id}] {self.song} ~ {self.tag}'
