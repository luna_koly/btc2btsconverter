import re

from django.test import TestCase

from .models import Song, Album, Tag

def is_empty(text):
    return bool(re.search('^\s*$', text))

class SongModelTests(TestCase):
    def test_has_some_songs(self):
        self.assertIs(Song.objects.all().count() > 0, True, f'No songs found')

    def test_all_songs_have_non_empty_titles(self):
        for it in Song.objects.all():
            is_valid = not is_empty(it.title)
            self.assertIs(is_valid, True, f'Song [{it.id}] contains an empty title > `{it.title}`')

    def test_all_songs_have_valid_youtube_links(self):
        for it in Song.objects.all():
            is_valid = bool(re.search('^https://youtu.be/[-0-9a-zA-Z_]+$', it.youtube))
            self.assertIs(is_valid, True, f'Song {it} contains an invalid youtube link > `{it.youtube}`')

class AlbumModelTests(TestCase):
    def test_all_albums_have_non_empty_titles(self):
        for it in Album.objects.all():
            is_valid = not is_empty(it.title)
            self.assertIs(is_valid, True, f'Album [{it.id}] contains an empty title > `{it.title}`')

    def test_all_albums_have_valid_covers(self):
        for it in Album.objects.all():
            is_valid = it.cover is not None
            self.assertIs(is_valid, True, f'Album {it} has no cover')

class TagModelTests(TestCase):
    def test_all_tags_have_non_empty_titles(self):
        for it in Tag.objects.all():
            is_valid = not is_empty(it.title)
            self.assertIs(is_valid, True, f'Tag [{it.id}] contains an empty title > `{it.title}`')
