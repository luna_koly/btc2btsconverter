from django.urls import include, path
from rest_framework import routers
import btc2bts.views

from . import views

resources_router = routers.DefaultRouter()
resources_router.register(r'users', views.UserViewSet)
resources_router.register(r'groups', views.GroupViewSet)
resources_router.register(r'albums', btc2bts.views.AlbumViewSet)
resources_router.register(r'tags', btc2bts.views.TagViewSet)
resources_router.register(r'songs', btc2bts.views.SongViewSet)
resources_router.register(r'songtags', btc2bts.views.SongTagViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.

urlpatterns = [
    path('resources/', include(resources_router.urls)),
    path('random-bts-song/', btc2bts.views.RandomBtsSong.as_view()),
    path('btc2bts/', btc2bts.views.Btc2Bts.as_view()),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
