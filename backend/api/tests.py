from rest_framework.test import APITransactionTestCase, APIRequestFactory

from btc2bts.views import Btc2Bts, Song
from btc2bts.models import Song

class ApiTests(APITransactionTestCase):
    def get_all_songs(self):
        return Song.objects.all()

    def test_btc2bts(self):
        results = set()

        factory = APIRequestFactory()
        view = Btc2Bts.as_view()

        for it in range(100):
            request = factory.post('/api/btc2bts/', {
                'value': str(it / 10),
            }, format='json')

            response = view(request)
            results.add(response.data['title'])

        inaccessible_songs = set()
        all_songs_count = 0

        for it in self.get_all_songs():
            all_songs_count += 1

            if it.title not in results:
                inaccessible_songs.add(it)

        self.assertIs(len(inaccessible_songs) == 0, True, f"Some songs are inaccessible within the testing range > {inaccessible_songs}")
